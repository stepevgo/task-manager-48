package ru.t1.stepanishchev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserDTORepository extends IAbstractDTORepository<UserDTO> {

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findOneByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    void clear();

}